/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

/**
 *
 * @author baba_
 */
public class Calculatrice {
    
    Calculatrice() {}
    
    public int addition(int a, int b)
    {
        return a+b;
    }
    
    public int soustraction(int a, int b)
    {
        return a-b;
    }
    
    public int multiplication(int a, int b)
    {
        return a*b;
    }
    
    public int division(int a, int b)
    {
        return a/b;
    }
    
    public int calcul(int a, int b, String operateur)
    {
        switch(operateur)
        {
            case("+"):
                return a+b;
            case("-"):
                return a-b;
            case("*"):
                return a*b;
            case("/"):
                return a/b;
        }
        return 0;
    }
}
