/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author baba_
 */
public class Factoriels {
    
    public float factorielIt(int number)
    {
        int result = 0;
        for (int i = 1; i <= number; i++) {
            result += i;
        }
        return result;
    }
    
    public float factorielRe(int number)
    {
        if (number == 0)
        {
            return number;
        }
        return factorielRe(number-1)+number;
    }
    
    public boolean timer(int t)
    {
        try {
            Thread.sleep(t * 1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Factoriels.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
}
