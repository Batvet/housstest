/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 *
 * @author baba_
 */
public class CalculatriceTest {
    
    @DisplayName("Test Calculatrice.addition()")
    @Test
    void testAddition() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        int c = a+b;
        
        // Act
        int result = calc.addition(a, b);
        
        // Assert
        assertThat(c).isEqualTo(result);
    }
    
    @DisplayName("Test Calculatrice.soustraction()")
    @Test
    void testSoustraction() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        int c = a-b;
        
        // Act
        int result = calc.soustraction(a, b);
        
        // Assert
        assertThat(c).isEqualTo(result);
    }
    
    @DisplayName("Test Calculatrice.multiplication()")
    @Test
    void testMultiplication() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        int c = 15;
        int d = 17;
        
        // Act
        int result = calc.multiplication(a, b);
        
        // Assert
        assertThat(c).isEqualTo(result);
        assertThat(d).isNotEqualTo(result);

    }
    
    @DisplayName("Test Calculatrice.division()")
    @Test
    void testDivision() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        int c = a/b;
        
        // Act
        int result = calc.division(a, b);
        
        // Assert
        assertThat(c).isEqualTo(result);
    }
    
    @DisplayName("Test Calculatrice.calcul()")
    @Test
    void testCalcul() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        String op = "+";
        int c = a+b;
        
        // Act
        int result = calc.calcul(a, b, op);
        
        // Assert
        assertThat(c).isEqualTo(result);
    }
    
    
    /* ______________ JUnit : ______________ 
    
    @DisplayName("Test Calculatrice.addition()")
    @Test
    void testAddition() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        int c = a+b;
        
        // Act
        int result = calc.addition(a, b);
        
        // Assert
        assertEquals(c, result);
    }
    
    @DisplayName("Test Calculatrice.soustraction()")
    @Test
    void testSoustraction() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        int c = a-b;
        
        // Act
        int result = calc.soustraction(a, b);
        
        // Assert
        assertEquals(c, result);
    }
    
    @DisplayName("Test Calculatrice.multiplication()")
    @Test
    void testMultiplication() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        int c = a*b;
        
        // Act
        int result = calc.multiplication(a, b);
        
        // Assert
        assertEquals(c, result);
    }
    
    @DisplayName("Test Calculatrice.division()")
    @Test
    void testDivision() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        int c = a/b;
        
        // Act
        int result = calc.division(a, b);
        
        // Assert
        assertEquals(c, result);
    }
    
    @DisplayName("Test Calculatrice.calcul()")
    @Test
    void testCalcul() {
        // Arrange
        Calculatrice calc = new Calculatrice();
        int a = 3;
        int b = 5;
        String op = "+";
        int c = a+b;
        
        // Act
        int result = calc.calcul(a, b, op);
        
        // Assert
        assertEquals(c, result);
    }
    
    */
}
