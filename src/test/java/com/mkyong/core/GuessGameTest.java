/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 *
 * @author baba_
 */
public class GuessGameTest {
    
    @DisplayName("Test invalid entry GuessGame.determineGuess()")
    @Test
    void testDetermineGuessInvalid() {
        // Arrange
        GuessGame gg = new GuessGame();
        int a = 101;
        int b = 39;
        int c = 5;
        String expected = "Your guess is invalid";
        
        // Act
        String result = gg.determineGuess(a, b, c);
        
        // Assert
        assertThat(expected).isEqualTo(result);
    }
    
    @DisplayName("Test correct guess GuessGame.determineGuess()")
    @Test
    void testDetermineGuessCorrect() {
        // Arrange
        GuessGame gg = new GuessGame();
        int a = 39;
        int b = 39;
        int c = 5;
        String expected = "Correct!\nTotal Guesses: 5";
        
        // Act
        String result = gg.determineGuess(a, b, c);
        
        // Assert
        assertThat(expected).isEqualTo(result);
    }
    
    @DisplayName("Test higher guess GuessGame.determineGuess()")
    @Test
    void testDetermineGuessHigher() {
        // Arrange
        GuessGame gg = new GuessGame();
        int a = 40;
        int b = 39;
        int c = 5;
        String expected = "Your guess is too high, try again.\nTry Number: 5";
        
        // Act
        String result = gg.determineGuess(a, b, c);
        
        // Assert
        assertThat(expected).isEqualTo(result);
    }
    
    @DisplayName("Test higher guess GuessGame.determineGuess()")
    @Test
    void testDetermineGuessLower() {
        // Arrange
        GuessGame gg = new GuessGame();
        int a = 38;
        int b = 39;
        int c = 5;
        String expected = "Your guess is too low, try again.\nTry Number: 5";
        
        // Act
        String result = gg.determineGuess(a, b, c);
        
        // Assert
        assertThat(expected).isEqualTo(result);
    }
}
