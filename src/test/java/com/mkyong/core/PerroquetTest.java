/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 *
 * @author baba_
 */
public class PerroquetTest {
    
    @DisplayName("Test Perroquet.perroquetParlant()")
    @Test
    void testPerroquetParlant() {
        // Arrange
        Perroquet p = new Perroquet();
        String testString = "je suis un perroquet";
        
        // Act
        String result = p.perroquetParlant(testString);
        
        // Assert
        assertEquals(testString, result);
    }
    
    @DisplayName("Test Perroquet.perroquetBonjour()")
    @Test
    void testPerroquetBonjour() {
        // Arrange
        Perroquet p = new Perroquet();
        String nom = "Mkyong";
        String testNom = "Bonjour Mkyong";
        
        // Act
        String result = p.perroquetBonjour(nom);
        
        // Assert
        assertEquals(testNom, result);
    }
}
