/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mkyong.core;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 *
 * @author baba_
 */
public class FactorielsTest {
    
    @BeforeAll
    static void doBeforeAll()
    {
        System.out.println("Début des tests");
    }
    
    @AfterAll
    static void doAfterAll()
    {
        System.out.println("Fin des tests");
    }
    
    @BeforeEach
    void doBeforeEach()
    {
        System.out.println("Début du test");
    }
    
    @AfterEach
    void doAfterEach()
    {
        System.out.println("Fin du test");
    }
    
    @DisplayName("Test Factoriels.factorielIt()")
    @Test
    void testFactorielIt()
    {
        // Arrange
        Factoriels facto = new Factoriels();
        int a = 3;
        float c = 6;
        
        // Act
        float result = facto.factorielIt(a);
        
        // Assert
        assertEquals(c, result);
    }
    
    /*
    @ParameterizedTest
    @CsvSource({"3, 6", "4, 10"})
    @DisplayName("Test Factoriels.factorielIt()")
    @Test
    void testFactorielRe(int input, float expected)
    {
        // Arrange
        Factoriels facto = new Factoriels();
        int a = input;
        float c = expected;
        
        // Act
        float result = facto.factorielRe(a);
        
        // Assert
        assertEquals(c, result);
    }
    */
    
    @Disabled
    @Timeout(30)
    @DisplayName("Test Factoriels.timer()")
    @Test
    void testTimer()
    {
        Factoriels facto = new Factoriels();
        int t = 20;
        boolean b = true;
        
        boolean result = facto.timer(t);
        
        assertEquals(b, result);
    }
}
